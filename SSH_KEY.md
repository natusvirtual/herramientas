* Open PuttyGen
* Haga clic en cargar
* Cargue su clave privada
* Ve Conversions->Export OpenSSHy exporta tu clave privada
* chmod 400 ~/.ssh/id_rsa
* Cree la versión RFC 4716 de la clave pública usando ssh-keyge
* ssh-keygen -e -f ~/.ssh/id_dsa > ~/.ssh/id_dsa_com.pub
* Convierta la versión RFC 4716 de la clave pública al formato OpenSSH:
* ssh-keygen -i -f ~/.ssh/id_dsa_com.pub > ~/.ssh/id_dsa.pub
