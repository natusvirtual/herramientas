### Instalando ###
* sudo apt-get install postgresql postgresql-contrib

###  Ingresando a postgres desde una terminal ###
* sudo -u postgres psql postgres

###  Configurar el password para el usuario postgresql ###
*  \password postgres 

### Instalando pgadmin ###
* sudo apt-get install pgadmin3