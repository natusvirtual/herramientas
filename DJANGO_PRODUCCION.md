# DJANGO EN PRODUCCION #

### Comandos para instalar en ubuntu ###

* sudo apt-get install python3
* sudo apt-get install python3-pip
* sudo apt-get install nginx
* sudo pip3 install virtualenv
* sudo pip3 install uwsgi 

### Creamos un usuario ###
* adduser deploy
* su - deploy

### Creamos la carpeta de la maquina virtual ###
* mkdir projects

### Creamos la carpeta para el proyecto de django ###
* mkdir web

### Creamos la virtualenv y la activamos ###
* virtualenv maquinaenv
* cd maquinaenv
* cd bin
* source activate

### Nos diregimos a la carpeta web y ejecutamos lo siguiente ###
* pip install django
* django-admin startproject sistema

### Configuramos el archivo settings.py ###
* ALLOWED_HOSTS = ['172.25.0.103']
* STATIC_URL = '/static/'
* STATIC_ROOT = os.path.join(BASE_DIR, 'static/')
### Juntamos los estaticos en la carpeta que se indico en el settings.py ###
* ./manage.py collectstatic

### desactivamos la maquina virtual ###
* deactivate

### Hacemos un test al sistema ###
* uwsgi --master --http :5000 --home /home/deploy/projects/maquinaenv --chdir /home/deploy/web/sistema --module sistema.wsgi:application

### Creamos un servicio automatico uwsgi
* vi /etc/systemd/system/uwsgi.service

```
[Unit]
Description=uWSGI Emperor

[Service]
ExecStartPre=/bin/bash -c 'mkdir -p /run/uwsgi; chown deploy:www-data /run/uwsgi'
ExecStart=/usr/local/bin/uwsgi --emperor /etc/uwsgi/sites
Restart=always
KillSignal=SIGQUIT
Type=notify
StandardError=syslog
NotifyAccess=all

[Install]
WantedBy=multi-user.target
```

### Creamos el archivo de configuracion para uwsgi
* vi /etc/uwsgi/sites/maquinaenv.ini

```
[uwsgi]
uid = deploy
base = /home/%(uid)

chdir = %(base)/web/sistema
home = %(base)/projects/maquinaenv
module = sistema.wsgi:application

master = true
processes = 5

socket = /run/uwsgi/maquinaenv.sock
chown-socket = %(uid):www-data
chmod-socket = 660
vacuum = true
```

### Reiniciamos el servicio de uwsgi ###
*  systemctl restart uwsgi.service
### Creamos el archivo de configuracion nginx ###
* /etc/nginx/sites-available/maquinaenv
```
 server {
  listen 9090;
  server_name 172.25.0.103;
  location = /favicon.ico { access_log off; log_not_found off; }
  location /static/ {
    root /home/deploy/web/sistema;
  }
  location / {
    include uwsgi_params;
    uwsgi_pass unix:/run/uwsgi/maquinaenv.sock;
  }
}
```
### sym-link to sites-enable ###
*  ln -s /etc/nginx/sites-available/maquinaenv /etc/nginx/sites-enabled/

### Reiniciamos nginx ###
*  systemctl restart nginx



