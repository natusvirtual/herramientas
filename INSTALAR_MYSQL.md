## INSTALAR MYSQL EN UBUNTU ##


* sudo apt-get install mysql-server mysql-client

### Para crear un nuevo usuario ###
* CREATE USER 'nombre_usuario'@'localhost' IDENTIFIED BY 'tu_contrasena';

### Usuario con todos los privilegios ###
* GRANT ALL PRIVILEGES ON * . * TO 'nombre_usuario'@'localhost';

### Refrescar los privilegios ###
* FLUSH PRIVILEGES;

### Puertos firewall ###
*  sudo ufw allow out 3306/tcp  
*  sudo ufw allow in 3306/tcp   
